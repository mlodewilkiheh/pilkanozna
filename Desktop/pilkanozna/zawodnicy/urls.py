from django.conf.urls import url
from . import views

app_name = 'zawodnik'

urlpatterns = [
    # /zawodnik/
    url(r'^$', views.IndexView.as_view(), name='index_z'),

    #/zawodnik/id/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    url(r'dodaj/$',views.PlayerCreate.as_view(), name='add-player'),
]