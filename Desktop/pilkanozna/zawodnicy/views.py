from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Zawodnik

class IndexView(generic.ListView):
    template_name = 'zawodnicy/index_z.html'

    def get_queryset(self):
        return Zawodnik.objects.all()

class DetailView(generic.DetailView):
    model = Zawodnik
    template_name = 'zawodnicy/detail.html'

class PlayerCreate(CreateView):
    model = Zawodnik
    fields = ['druzyna', 'imie', 'nazwisko', 'wiek', 'wzrost', 'pozycja', 'numer','zdjecie']

