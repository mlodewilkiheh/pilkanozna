from django.db import models
from django.core.urlresolvers import reverse
from druzyna.models import Druzyna


class Zawodnik(models.Model):
    druzyna = models.ForeignKey(Druzyna, on_delete=models.CASCADE)
    imie = models.CharField(max_length=50)
    nazwisko = models.CharField(max_length=70)
    wiek = models.IntegerField()
    wzrost = models.IntegerField()
    pozycja = models.CharField(max_length=70)
    numer = models.IntegerField()
    zdjecie = models.FileField()

    def __str__(self):
        return self.imie + '    ' + self.nazwisko

    def get_absolute_url(self):
        return reverse('zawodnik:detail', kwargs={'pk': self.pk})


