# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-01 22:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zawodnicy', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zawodnik',
            name='czy_ulub',
        ),
        migrations.AddField(
            model_name='zawodnik',
            name='zdjecie',
            field=models.FileField(default=1, upload_to=''),
            preserve_default=False,
        ),
    ]
