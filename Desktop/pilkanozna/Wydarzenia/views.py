from django.utils import timezone
from django.views import generic

from .models import Event


class IndexView(generic.ListView):
    template_name = 'events/index.html'
    context_object_name = 'latest_events_list'

    def get_queryset(self):

        return Event.objects.filter(
            created__lte=timezone.now()
        ).order_by('-created')[:5]

class DetailView(generic.DetailView):
    model = Event
    template_name = 'events/detail.html'
    def get_queryset(self):
        return Event.objects.filter(created__lte=timezone.now())



# Create your views here.
