from django.urls import reverse
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import datetime

class Place(models.Model):
    #Dane miejsca zawodow
    name = models.CharField(max_length=75)
    adress = models.TextField()
    description = models.TextField()

    class Meta:
        verbose_name = u"Place"
        verbose_name_plural = u"Place"
        ordering = ['name']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pub_date = None

    def __str__(self):
        return self.name



class Category(models.Model):
    #Rodzaj zawodow - mecz towarzyski, ligowy, pucharowy
    name = models.CharField(max_length=75)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    #Wydarzenie
    name = models.CharField(max_length=75)

    def __str__(self):
        return self.name

    home_team_goals = models.CharField(max_length=5, default=0)
    away_team_goals = models.CharField(max_length=5, default=0)
    date = models.DateField()
    starts = models.TimeField(blank=True, null=True)
    ends = models.TimeField(blank=True, null=True)
    price = models.CharField(max_length=20)
    place = models.ForeignKey(Place)
    category = models.ForeignKey(Category)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)

    class Meta:
        verbose_name = u"Wydarzenie"
        verbose_name_plural = u"Wydarzenia"
        ordering = ['name']


def get_absolute_url(self):
    return reverse('events:detail', kwargs={'pk': self.pk})


