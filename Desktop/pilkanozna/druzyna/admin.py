from django.contrib import admin
from zawodnicy.models import Zawodnik
from .models import Druzyna

admin.site.register(Zawodnik)
admin.site.register(Druzyna)

