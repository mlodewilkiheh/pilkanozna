from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Druzyna
from zawodnicy.models import Zawodnik


class IndexView(generic.ListView):
    template_name = 'druzyna/index.html'
    model = Zawodnik

    def get_queryset(self):
        return Druzyna.objects.all()


class DetailView(generic.DetailView):
    model = Druzyna
    template_name = 'druzyna/detail.html'


class TeamsCreate(CreateView):
    model = Druzyna
    fields = ['nazwa','liga','kraj','trener','logo']

class TeamsDelete(DeleteView):
    model = Druzyna
    success_url = reverse_lazy('druzyna:index')

class TeamsUpdate(UpdateView):
    model = Druzyna
    fields = ['nazwa', 'liga', 'kraj', 'trener', 'logo']