from django.db import models
from django.core.urlresolvers import reverse
import django_tables2 as tables

class Druzyna(models.Model):
    nazwa = models.CharField(max_length=100)
    liga = models.CharField(max_length=100)
    kraj = models.CharField(max_length=50)
    trener = models.CharField(max_length=200)
    logo = models.FileField()


    def __str__(self):
        return self.nazwa


    def get_absolute_url(self):
        return reverse('druzyna:detail', kwargs={'pk': self.pk})







