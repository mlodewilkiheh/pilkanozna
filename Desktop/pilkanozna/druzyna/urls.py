from django.conf.urls import url
from . import views

app_name = 'druzyna'

urlpatterns = [
    # /druzyna/
    url(r'^$', views.IndexView.as_view(), name='index'),

    # /druzyna/<druzyna_id>/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    url(r'dodaj/$',views.TeamsCreate.as_view(), name='add-team'),

    #/druzyna/aktualizuj/1/
    url(r'aktualizuj/(?P<pk>[0-9]+)/$',views.TeamsUpdate.as_view(), name='update-team'),

    url(r'(?P<pk>[0-9]+)/usun/$',views.TeamsDelete.as_view(), name='delete-team'),

]
